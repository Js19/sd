        .data
sign1:  .byte 0b0
sign2:  .byte 0b1
exp1:   .word 0b000000001100100  @0x7D  15 bits
exp2:   .word 0b000000001100100  @0x85  15 bits
m1:     .word 0b0000000000000000 @16 bits
m2:     .word 0b0000000000000000 @16 bits

        .text
        .globl main
    
main:   
        ldr r0, =exp1       @load the address of exponent
        ldr r1, [r0]        @load the value of exponent  
        ldr r2, =exp2       @load the address of the exponent
        ldr r3, [r2]        @load the value of other exponent
        ldr r5, =m1         @load the address of the mantissa from first number
        ldr r6, [r5]        @load the value of the mantissa
        ldr r7, =m2         @load the address of the mantissa from the second number
        ldr r8, [r7]        @load the value of the other mantissa


        cmp r3, r1          @if(exp2 < exp1){
        bgt greater         
        sub r4, r1, r3      @exp1 - exp2
        mov r11, r1 		@store the exponent
        mov r7,r7,LSL r4    @shift right the mantissa y - x (left to lower the exponent)
        b continue          @do not want to subtract and shift again
greater:                    @}else{
        sub r4, r3, r1      @exp2 - exp1
        mov r11, r3 		@store the exponent 
        mov r5,r5,LSR r4    @shift left the mantissa x - y (right to raise the exponent)
continue:
        add r9, r6, r8      @do the addition when the exponents match
        ldrb r0, =sign1		@load the sign value 
        ldrb r1, [r0]		@''
        ldrb r2, =sign2		@''
        ldrb r10, [r2]		@''

        add r3, r1, r10		@add the 2 sign values. 

        @sign is in r3
        @exponent is in r11
        @mantissa is in r9


    
        






